FROM node:lts-slim

COPY . .
RUN yarn
RUN yarn build:webapp


FROM nginx:alpine

COPY --from=0 packages/ganttlab-adapter-webapp/dist /usr/share/nginx/html
